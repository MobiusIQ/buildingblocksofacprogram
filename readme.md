# building blocks of a proram

# some basics

`make` - build the application completely
`make clean` - remove all intermediate files
`make run` - execute the application and display the return code
`make debug` - start the application in GDB in TUI mode

`make main.ii` - run the preprocessor only
`make main.s` - preprocess and compile the code to assembly
`make main.o` - preprocess, compile, and assemble the code to an object file
`make main` - preprocess, compile, assemble, and link the object file into an application

## makefile

Nice examples of a basic makefile with just enough flexiblity

## preprocess

Look at what the preprocessor output looks like

## compile

Convert the preprocessed file into assembly

## assemble

Assemble the machine code into an object file

## link

Link the object files into a final executable

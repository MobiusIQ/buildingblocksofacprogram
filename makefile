MAKEFLAGS += --no-builtin-rules --no-builtin-variables

CC = g++
CFLAGS = -Wall -g

# list header files here
DEPS = main.hpp
# list directories that contain header files (only needed for the preprocessor)
INC = .

# default rule.  run if only make is called
all : main

# for the simple main, run this target to see the return value
run : main
	-(./main && echo $$?) || echo $$?

debug : main
	gdb main --tui

# list dependencies after the make target

# link the objects into the final application
# needs an object file to produce the main app
# we also let make know that it should be aware of the $(DEPS) when making this
# file so if a header changes, we re-run the make chain)
main : main.o 
	$(CC) $(CFLAGS) $^ -o $@

# assemble the file into an object file
# to make an object file, we need an assembly file
%.o : %.s 
	$(CC) $(CFLAGS) -c $< -o $@
	
# compile the preprocessed file into assembly
# to make an assembly file, we need a preprocessed file
%.s : %.ii 
	@# "-S" to assemble
	@# "-fverbose-asm" to provide useful comments into the assembled output
	$(CC) $(CFLAGS) -S -fverbose-asm $< -o $@

# create a preprocessed file with an extension that
# g++ recognizes should *not* be preprocessed when compiled later
# to make a preprocessed file, we need a cpp file
%.ii : %.cpp $(DEPS)
	@# "-E" preprocess only
	@# "-P" supress linemarkers in the output file
	$(CC) $(CFLAGS) -I$(INC) -E -P $< -o $@

clean :
	@# "-" ignore error on excution
	@# "@" supress the echo of the command
	@# "2>/dev/null" pipe all error messages to /dev/null
	@# "|| true" if the first command fails, execute true to prevent the
	@#  message that an error was ignored from ocurring
	-@rm *.ii *.s *.o main 2>/dev/null || true

# Phony targets have no dependencies and are always run
.PHONY: clean run

# keep intermediate files
.PRECIOUS: %.ii %.s %.o

# remove implicit rules so that my rules are run
# .SUFFIXES:
